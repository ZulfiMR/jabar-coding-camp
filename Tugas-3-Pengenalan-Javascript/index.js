/* Soal 1
buatlah variable-variabel seperti di bawah ini

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

gabungkan variabel-variabel tersebut agar menghasilkan output

saya senang belajar JAVASCRIPT
*/

/*

jawaban soal 1
*/
var step1 = "saya senang ";
var step2 = "belajar javascript";

console.log(step1.concat(step2));

/*soal 2
buatlah variabel-variabel seperti di bawah ini

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
*catatan :
1. gunakan 3 operasi, tidak boleh  lebih atau kurang. contoh : 10 + 2 * 4 + 6
2. Boleh menggunakan tanda kurung . contoh : (10 + 2 ) * (4 + 6)
*/

/*
jawaban soal 2
*/
var katapertama = Number("10");
var kataKedua = Number("2");
var kataKetiga = Number("4")
var kataKeempat = Number("6")
console.log((katapertama+kataKedua)*(kataKeempat-kataKetiga));

/* soal 3
buatlah variabel-variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua; // do your own! 
var kataKetiga; // do your own! 
var kataKeempat; // do your own! 
var kataKelima; // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

Kata Pertama: wah
Kata Kedua: javascript
Kata Ketiga: itu
Kata Keempat: keren
Kata Kelima: sekali
*/

/* 
jawaban soal 3
*/

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0,3); 
var kataKedua = kalimat.substr(4,10); 
var kataKetiga = kalimat.substr(15,3);
var kataKeempat = kalimat.substr(19,5);
var kataKelima = kalimat.substr(25,6);

console.log('kata pertama:' + kataPertama);
console.log('kata kedua:' + kataKedua);
console.log('kata ketiga:' + kataKetiga);
console.log('kata keempat:' + kataKeempat);
console.log('kata kelima:' + kataKelima);